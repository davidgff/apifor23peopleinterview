let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');

let app = express();

// Importing routes
let apiRoutes = require("./routes/api-routes");

app.use(bodyParser.urlencoded({
    extended: true
}));
 
// parse application/json
app.use(bodyParser.json());

//mongoose.connect('mongodb://localhost/23PeopleInterview', { useNewUrlParser: true});
//mongoose.connect('mongodb+srv://apifor23peopleinterview:<*apifor23peopleinterview*>@cluster0-5s89e.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true});
mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://127.0.0.1:27017/apifor23peopleinterview');

var db = mongoose.connection;

if(!db)
    console.log("Error connecting db")
else
    console.log("Db connected successfully")

var port = process.env.PORT || 8080;

app.get('/', (req, res) => res.send('Hola, la ruta hacia la API es /api'));

app.use('/api', apiRoutes);

app.listen(port, function () {
     console.log("corriendo app en puerto " + port);
});
