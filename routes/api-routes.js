let router = require('express').Router();

router.get('/', function (req, res) {
    res.json({
        status: 'API para prueba por entrevista 23People',
        message: 'Ruta /contacts',
    });
});

// Import contact controller
var contactController = require('../controllers/contactController');

// Contact routes
router.route('/contacts')
    .get(contactController.index)
    .post(contactController.new);

router.route('/contacts/:contact_id')
    .get(contactController.view)
    .put(contactController.update)
    .delete(contactController.delete);

module.exports = router;
