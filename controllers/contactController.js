// contactController.js

// Import contact model
Contact = require('../models/contactModel');
const { validate, clean, format } = require('rut.js');

// Handle index actions
exports.index = function (req, res) {
    Contact.get(function (err, contacts) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }

        res.json({
            status: "Exito",
            message: "Coleccion Contactos obtenida exitosamente",
            data: contacts
        });

    });
};

// Handle create contact actions
exports.new = function (req, res) {
    var contact = new Contact();
    contact.rut = req.body.rut ? req.body.rut : contact.rut;
    contact.name = req.body.name;
    contact.lastName = req.body.lastName;
    contact.edad = req.body.edad;

    // first let's validate if it is a valid rut
    if (!validate(contact.rut)){
        res.status(400).send({ 
            message: 'El RUT es inválido',
            data: contact
        });
    } else{
        // save the contact and check for errors
        contact.save(function (err) {
            if (err){
                res.status(400).send({ 
                    message: err,
                });   
            }
            else {
                res.status(201).send({ 
                    message: 'Contacto creado',
                    data: contact
                });
            }               
        });
    }
};

// Handle view contact info
exports.view = function (req, res) {
    Contact.findById(req.params.contact_id, function (err, contact) {
        if (err){
            res.status(404).send({ 
                message: 'No se encontro Contacto',
            });
        } else {
            res.json({
                message: 'Cargando detalles de Contacto',
                data: contact
            });    
        }
    });
};

// Handle update contact info
exports.update = function (req, res) {
    if (req.params.contact_id.match(/^[0-9a-fA-F]{24}$/)) {

        Contact.findById(req.params.contact_id, function (err, contact) {

            if (err){
                res.send(err);
            }
    
            if (typeof contact != 'undefined'){
                contact.rut = req.body.rut ? req.body.rut : contact.rut;
                contact.name = req.body.name;
                contact.lastName = req.body.lastName;
                contact.edad = req.body.edad;

                // check for errors, then save the contact

                if (!validate(contact.rut)){
                    res.status(400).send({ 
                        message: 'El RUT es inválido',
                    });
                }else{
                    contact.save(function (err) {
                        if (err){
                            res.status(400).send({
                                message: 'Error - ' + err.name + ' ' + err.message,
                            });
                        } else {
                            res.json({
                                message: 'Contacto actualizado',
                                data: contact
                            });
                        }
                    });
                }
            } else {
                res.status(404).send({ 
                    message: 'No se encontro Contacto',
                });
            }
        });
    } else {
        res.status(404).send({ 
            message: 'No se encontro Contacto',
        });
    }
};

// Handle delete contact
exports.delete = function (req, res) {
    if (req.params.contact_id.match(/^[0-9a-fA-F]{24}$/)) {
        Contact.findById(req.params.contact_id, function (err, contact) {
            if (err){
                res.send(err);
            }
    
            if (typeof contact != 'undefined'){
                Contact.remove({
                    _id: req.params.contact_id
                }, function (err, contact) {
                    if (err){
                        res.send(err);
                    } else {
                        res.status(204).send();
                    }
                });
            } else {
                res.status(404).send({ 
                    message: 'No se encontro Contacto',
                });
            }
        });
    } else {
        res.status(404).send({ 
            message: 'No se encontro Contacto',
        });
    }
};

    


