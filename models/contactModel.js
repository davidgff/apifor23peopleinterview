// contactModel.js
var mongoose = require('mongoose');

// Setup schema
var contactSchema = mongoose.Schema({
    rut: {
        type: String,
        required: true,
        max: 10,
        unique: true,
        validate: {
            validator: function(v) {
                return /^0*(\d{1,3}(\d{3})*)\-?([\dkK])$/.test(v);
              },
            message: 'RUT con formato invalido'
        }
    },
    name: {
        type: String,
        min: 1,
        required: true
    },
    lastName: {
        type: String,
        min: 1,
        required: true
    },
    edad: {
        type: Number,
        required: true,
        validate: {
            validator: Number.isInteger,
            message   : '{VALUE} is not an integer value'
        },
        min: 1
    },
});

// Export Contact model
var Contact = module.exports = mongoose.model('contact', contactSchema);

module.exports.get = function (callback, limit) {
    Contact.find(callback).limit(limit);
}
